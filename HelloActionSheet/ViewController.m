//
//  ViewController.m
//  HelloActionSheet
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/16.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toClick:(id)sender {
    UIActionSheet *myActionSheet = [[UIActionSheet alloc]initWithTitle:@"myActionSheet" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Share", @"Save", nil];
    
    [myActionSheet showInView:self.view];
}

//Copy from the protocol UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"You pressed %ld", (long)buttonIndex);
}
@end
